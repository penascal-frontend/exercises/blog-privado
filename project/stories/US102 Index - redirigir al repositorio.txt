Código: US102
Nombre: Index - redirigir al repositorio
Roles: Público general
Necesidad:
  Tener un punto de acceso al código del repositorio

Razón:
  Es impotante dar visibilidad al proyecto a programadores que no lo conozcan y aportar a la comunidad

Aceptación:
  La página index debe de tener un link que muestre el repositorio en otra pestaña.

Relaciones:
