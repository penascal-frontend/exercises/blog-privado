import { JSDOM } from 'jsdom'
import { describe, expect, vi, it } from 'vitest'

describe('Script principal de article.html', () => {

    const article = { 
        id: '1', title: 'mock title', date: '1970-01-01', image: 'https://',
        author: { name: 'mock author', mail: 'mock@mail.com' },
        content : 'paragraph 1\nparagraph 2\n\paragraph 3',
        labels: []
    }
    const articles = [ article ]

    const user = { username: 'user1', password: 'pass1' }
    const session_id = '3yz'
    const users = [user]
    
    const sleep = ms => new Promise(_ => setTimeout(_, ms))

    beforeEach(() => {
        const dom = new JSDOM(/*html*/`
            <img id="image" src="" alt="">
            <main></main>
            <span id="date"></span> 
            <span id="author"></span>
            <a id="feedback"></a>
            <a id="logout"></a>
        `)
        global.window = dom.window
        global.document = window.document
        global.fetch = url => {
            return new Promise((resolve, _) => {
                resolve({
                    json: () => (
                        {
                            'articles.json': articles,
                            'users.json': users
                        }?.[url] ?? null
                    ),
                    ok: true
                })
            })
        }
        global.localStorage = {
            clear: vi.fn(),
            setItem: vi.fn(),
            getItem: key => ({ user: user.username, session: session_id }?.[key] ?? null)
        }
        global.location = { replace: vi.fn(), href: '', search: `?id=${article.id}`}
    })
    
    afterEach(() => { vi.resetModules() })

    it('Cargar contenido del articulo', async () => {
        const date = document.getElementById('date')
        const image = document.getElementById('image')
        const author = document.getElementById('author')
        const main = document.querySelector('main')
        const formatter = new Intl.DateTimeFormat('es-ES', { dateStyle: 'full' })
        const expected_date = formatter.format(new Date(article.date))

        await import('@/article.js')
        await sleep(10)
        const title = main.querySelector('h2')
        const paragraphs = Array.from(main.querySelectorAll('p'))
        
        expect(image.alt).toBe(article.title)
        expect(image.src).toBe(article.image)
        expect(date.innerText).toBe(expected_date)
        expect(author.innerText).toBe(article.author.name)
        expect(title.innerText).toBe(article.title)
        expect(paragraphs.map(p => p.innerText).join('\n')).toBe(article.content)
    })

    it('Habilitar feedback', async () => {
        const feedback = document.getElementById('feedback')
        const regexp = new RegExp(`^mailto:.*${encodeURI(article.title)}.*$`)

        await import('@/article.js')
        await sleep(10)
        
        expect(feedback.href).toMatch(regexp)
    })

    it('Validar session KO', async () => {
        localStorage.getItem = key => ({ user: 'user1', session: '' }[key])
        await import('@/article.js')
        await sleep(10)

        expect(localStorage.clear).toHaveBeenCalled()
        expect(location.replace).toHaveBeenCalledWith('login.html')
    })

    it('redirigir al blog si no se ha pasado un id', async () => {
        location.search = ''

        await import('@/article.js')
        await sleep(10)
        
        expect(location.replace).toHaveBeenCalledWith('blog.html')
    })

    it('redirigir al blog si se ha pasado un id que no existe', async () => {
        location.search = '?id=1234567890'

        await import('@/article.js')
        await sleep(10)
        
        expect(location.replace).toHaveBeenCalledWith('blog.html')
    })

    it('Proceso logout confirmado', async () => {
        vi.spyOn(window, 'confirm').mockImplementation((_) => true);
        global.confirm = window.confirm

        await import('@/article.js')
        await sleep(10)
        const button = document.getElementById('logout').click()
        
        expect(window.confirm).toHaveBeenCalled()
        expect(localStorage.clear).toHaveBeenCalled()
        expect(location.replace).toHaveBeenCalledWith('index.html')
    })

    it('Proceso logout cancelado', async () => {
        vi.spyOn(window, 'confirm').mockImplementation((_) => false);
        global.confirm = window.confirm

        await import('@/article.js')
        await sleep(10)
        const button = document.getElementById('logout').click()
        
        expect(window.confirm).toHaveBeenCalled()
        expect(localStorage.clear).not.toHaveBeenCalled()
        expect(location.replace).not.toHaveBeenCalled()
    })
})
