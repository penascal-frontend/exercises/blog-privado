import { JSDOM } from 'jsdom'
import { describe, expect, vi, it } from 'vitest'

describe('Script principal de login.html', () => {

    beforeEach(async () => {
        const dom = new JSDOM(/*html*/`
            <input type="checkbox" id="terms" />
            <button id="login" disabled></button>
        `)
    
        global.window = dom.window
        global.document = dom.window.document
        global.location = { replace: vi.fn() }
        global.localStorage = { clear: vi.fn(), getItem: () => null}
        global.fetch = vi.fn().mockResolvedValue({
            json: () => [{username: 'user1', password: 'pass1'}], // session = '3yx'
            ok: true
        })
        
    })
    
    afterEach(() => { vi.resetModules() })

    it('Validar session OK', async () => {
        localStorage.getItem = key => ({ user: 'user1', session: '3yz' }[key])
        await import('@/login.js')

        expect(location.replace).toHaveBeenCalledWith('blog.html')
    })

    it('Validar session KO', async () => {
        localStorage.getItem = key => ({ user: 'user1', session: '' }[key])
        await import('@/login.js')

        expect(localStorage.clear).toHaveBeenCalled()
    })

    it('Habilitar/deshabilitar botón [Acceder]', async () => {
        await import('@/login.js')
        
        const checkbox = document.getElementById('terms')
        const button = document.getElementById('login')
        expect(checkbox.checked).toBe(false)
        expect(button.disabled).toBe(true)
        checkbox.click()
        expect(button.disabled).toBe(false)
        checkbox.click()
        expect(button.disabled).toBe(true)
    })


})
