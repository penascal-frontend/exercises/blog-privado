import { describe, expect, vi, it } from 'vitest'

describe('Script principal de auth.html', () => {

    const user = {username: 'user1', password: 'pass1'}
    const session_id = '3yz'

    beforeEach(async () => {
        global.fetch = vi.fn().mockResolvedValue({ json: () => [user], ok: true })
        global.localStorage = { clear: vi.fn(), setItem: vi.fn() }
        global.location = { replace: vi.fn(), search: '' }
    })
    
    afterEach(() => { vi.resetModules() })

    it('Validar usuarios OK', async () => {
        location.search = `?username=${user.username}&password=${user.password}`
        await import('@/auth.js')

        expect(localStorage.setItem).toHaveBeenCalledTimes(2)
        expect(localStorage.setItem).toHaveBeenCalledWith('user', user.username)
        expect(localStorage.setItem).toHaveBeenCalledWith('session', session_id)
        expect(location.replace).toHaveBeenCalledWith('blog.html')
    })

    it('Validar usuarios KO: username erroneo', async () => {
        location.search = `?username=xxx&password=${user.password}`
        await import('@/auth.js')

        expect(localStorage.clear).toHaveBeenCalled()
        expect(location.replace).toHaveBeenCalledWith('login.html')
    })

    it('Validar usuarios KO: password erroneo', async () => {
        location.search = `?username=${user.username}&password=xxx`
        await import('@/auth.js')

        expect(localStorage.clear).toHaveBeenCalled()
        expect(location.replace).toHaveBeenCalledWith('login.html')
    })

    it('Validar usuarios KO: parámetros erroneos', async () => {
        location.search = `'?user=${user.username}&pass=${user.password}`
        await import('@/auth.js')

        expect(localStorage.clear).toHaveBeenCalled()
        expect(location.replace).toHaveBeenCalledWith('login.html')
    })
})
