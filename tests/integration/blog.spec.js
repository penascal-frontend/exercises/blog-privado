import { JSDOM } from 'jsdom'
import { describe, expect, vi, it } from 'vitest'

describe('Script principal de blog.html', () => {

    const article = { 
        id: '1', title: '', date: '1970-01-01', image: '',
        author: { name: '', mail: '' },
        content : '',
        labels: []
    }
    const articles = [
        {...article, id:'1', labels:['a', 'b']},
        {...article, id:'2', labels:['b', 'c']},
        {...article, id:'3', labels:['c', 'd']},
        {...article, id:'4', labels:['a', 'c']},
    ]

    const user = { username: 'user1', password: 'pass1' }
    const session_id = '3yz'
    const users = [user]
    
    const sleep = ms => new Promise(_ => setTimeout(_, ms))

    beforeEach(() => {
        const dom = new JSDOM(/*html*/`
            <section id="articles"></section>
            <ul id="labels"></ul>
            <a id="logout"></a>
        `)
        global.window = dom.window
        global.document = window.document
        global.fetch = url => {
            return new Promise((resolve, _) => {
                resolve({
                    json: () => (
                        {
                            'articles.json': articles,
                            'users.json': users
                        }?.[url] ?? null
                    ),
                    ok: true
                })
            })
        }
        global.localStorage = {
            clear: vi.fn(),
            setItem: vi.fn(),
            getItem: key => ({ user: user.username, session: session_id }?.[key] ?? null)
        }
        global.location = { replace: vi.fn(), href: '' }
    })
    
    afterEach(() => { vi.resetModules() })

    it('Cargar articulos', async () => {
        const container = document.querySelector('#articles')
        
        await import('@/blog.js')
        await sleep(10)
        
        expect(container.childNodes.length).toBe(articles.length)
    })

    it('Cargar etiquetas', async () => {
        const container = document.getElementById('labels')
        const labels = Array.from(new Set(
            articles.reduce((acum, elem) => [...acum, ...elem.labels], [])
        )).sort()

        await import('@/blog.js')
        await sleep(10)

        expect(container.childNodes.length).toBe(labels.length + 1)
    })

    it('Filtrar artículos', async () => {
        const container = document.getElementById('articles')
        const label_counts = articles.reduce((acum, elem) => ({
            ...acum,
            ...elem.labels.reduce((label_acum, label_elem) => ({
                ...label_acum,
                [label_elem]: (acum[label_elem] ?? 0) + 1
            }), {})
        }), {})

        await import('@/blog.js')
        await sleep(10)

        for (const label in label_counts) {
            const query = `#labels input[type=radio][value="${label}"]`
            const input = document.querySelector(query)
            await input.click()
            await sleep(10)  // because eventhandler is async
            expect(container.childNodes.length).toBe(label_counts[label])
        }
    })

    it('Validar session KO', async () => {
        localStorage.getItem = key => ({ user: 'user1', session: '' }[key])
        await import('@/blog.js')
        await sleep(10)

        expect(localStorage.clear).toHaveBeenCalled()
        expect(location.replace).toHaveBeenCalledWith('login.html')
    })

    it('Proceso logout confirmado', async () => {
        vi.spyOn(window, 'confirm').mockImplementation((_) => true);
        global.confirm = window.confirm

        await import('@/blog.js')
        await sleep(10)
        const button = document.getElementById('logout').click()
        
        expect(window.confirm).toHaveBeenCalled()
        expect(localStorage.clear).toHaveBeenCalled()
        expect(location.replace).toHaveBeenCalledWith('index.html')
    })

    it('Proceso logout cancelado', async () => {
        vi.spyOn(window, 'confirm').mockImplementation((_) => false);
        global.confirm = window.confirm

        await import('@/blog.js')
        await sleep(10)
        const button = document.getElementById('logout').click()
        
        expect(window.confirm).toHaveBeenCalled()
        expect(localStorage.clear).not.toHaveBeenCalled()
        expect(location.replace).not.toHaveBeenCalled()
    })

    it('mostrar contenido del artículo', async () => {
        const query = `#articles article[id="${articles[0].id}"]`
        const expected_url = `article.html?id=${articles[0].id}`

        await import('@/blog.js')
        await sleep(10)
        const button = document.querySelector(query).click()

        expect(location.href).toBe(expected_url)
    })
})
