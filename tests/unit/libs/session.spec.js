import { describe, expect, vi, it } from 'vitest'
import { makeSession, setSession, clearSession, checkSession } from '@/libs/session'

describe('Session functions', () => {
    const right_user = 'user1'
    const right_pass = 'pass1'
    const user = { username: right_user, password: right_pass }
    const users = [user]

    function createLocalStorageMock() {
        let store = {}

        return {
            getItem(key) {
                return store?.[key] ?? null
            },

            setItem(key, value) {
                store[key] = value
            },

            clear() {
                store = {}
            },

            removeItem(key) {
                delete store[key]
            },

            getAll() {
                return store
            }
        }
    }

    global.fetch = vi.fn()
    global.localStorage = createLocalStorageMock()

    function createFetchResponse(data, ok) {
        return {
            json: () => new Promise((resolve) => resolve(data)),
            ok
        }
    }

    it('test makeSession', async () => {
        const spec_session = '3yz'
        const user_session = makeSession(user)

        expect(user_session).toEqual(spec_session)
    })

    it('test setSession', async () => {
        const session = makeSession(user)
        setSession(user.username, session)

        expect(localStorage.getItem('user')).toEqual(user.username)
        expect(localStorage.getItem('session')).toEqual(session)
    })

    it('test clearSession', () => {
        clearSession()

        expect(localStorage.getItem('user')).toEqual(null)
        expect(localStorage.getItem('session')).toEqual(null)
    })

    it('test checkSession', async () => {
        const session = makeSession(user)
        fetch.mockResolvedValue(createFetchResponse(users, true))
        expect(await checkSession(user.username, session)).toBe(true)
        expect(await checkSession(user.username, 'xxxxx')).toBe(false)
    })
})
