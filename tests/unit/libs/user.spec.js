import { describe, expect, vi, it } from 'vitest'
import { getUsers, validateCredentials, getCredentialsFromParams } from '@/libs/user'

describe('Auth functions', () => {
    const right_user = 'user1'
    const right_pass = 'pass1'
    const wrong_user = 'user0'
    const wrong_pass = 'pass2'
    const users = [{ username: right_user, password: right_pass }]
    const search = `?username=${right_user}&password=${right_pass}`
    
    global.fetch = vi.fn()
    function createFetchResponse(data, ok) {
        return { 
            json: () => new Promise((resolve) => resolve(data)),
            ok
        }
    }

    it('test getUsers', async () => {
        fetch.mockResolvedValue(createFetchResponse(users, true))
        const res = await getUsers()

        expect(res).toEqual(users)
    })

    it('test validateCredentials', () => {
        expect(validateCredentials(users, right_user, right_pass)).toBe(true)
        expect(validateCredentials(users, right_user, wrong_pass)).toBe(false)
        expect(validateCredentials(users, wrong_user, right_pass)).toBe(false)
        expect(validateCredentials(users, wrong_user, wrong_pass)).toBe(false)
    })

    it('test getCredentialsFromParams', () => {
        const user = users[0]
        const search_string = `?username=${user.username}&password=${user.password}`
        
        const res = getCredentialsFromParams(search_string)
        expect(res).toEqual(user)
    })

})
