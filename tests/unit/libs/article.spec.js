import { describe, expect, vi, it } from 'vitest'
import { getArticles, getLabels } from '@/libs/article'

describe('Article functions', () => {
    const articles = [
        { labels: ['a', 'b'] },
        { labels: ['b', 'c'] },
        { labels: ['c', 'd'] },
        { labels: ['a', 'c'] },
    ]
    
    global.fetch = vi.fn()
    function createFetchResponse(data, ok) {
        return { 
            json: () => new Promise((resolve) => resolve(data)),
            ok
        }
    }

    it('test getArticles', async () => {
        fetch.mockResolvedValue(createFetchResponse(articles, true))
        const res = await getArticles()

        expect(res).toEqual(articles)
    })

    it('test getLabels', async () => {
        fetch.mockResolvedValue(createFetchResponse(articles, true))
        const res = await getLabels()

        expect(res).toEqual('abcd'.split(''))
    })

})