import { describe, expect, vi, it } from 'vitest'
import { getJsonData } from '@/libs/data'


describe('test library data.js', async () => {
    
    global.fetch = vi.fn()
    function createFetchResponse(data, ok) {
        return { 
            json: () => new Promise((resolve) => resolve(data)),
            ok
        }
    }

    it('test function getJsonData', async () => {
        const users = [{ username: 'user1', password: 'pass1' }]

        fetch.mockResolvedValue(createFetchResponse(users, true))
        const res = await getJsonData('users.json')

        expect(res).toEqual(users)

    })
})
