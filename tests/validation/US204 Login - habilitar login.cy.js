describe('US204 Login - habilitar Login', () => {
    const URL = `${Cypress.env('URL')}/login.html`

    function visit() {
        cy.visit(URL)
    }

    function getSelectors() {
        const check = 'input[type=checkbox]'
        const login = 'button[type=submit]'
        return { check, login }
    }

    it('Si el checkbox de las aceptación NO está marcado, el boton de login, NO ha de estar habilitado', () => {
        visit()
        const { check, login } = getSelectors()
        cy.get(check).should('not.be.checked')
        cy.get(login).should('be.disabled')
        cy.get(check).click()
        cy.get(check).click()
        cy.get(check).should('not.be.checked')
        cy.get(login).should('be.disabled')
    })

    it(
      'Si el checkbox de las aceptación SI está marcado, el boton de login, SI ha de estar habilitado',
      () => {
        visit()
        const { check, login } = getSelectors()
        cy.get(check).should('not.be.checked')
        cy.get(login).should('be.disabled')
        cy.get(check).click()
        cy.get(check).should('be.checked')
        cy.get(login).should('not.be.disabled')
        cy.get(check).click()
        cy.get(check).click()
        cy.get(check).should('be.checked')
        cy.get(login).should('not.be.disabled')
      }
    )
})
