describe('US503 Article - validar sesion', () => {
    const URL = `${Cypress.env('URL')}/article.html`
    
    const user = { username: 'user1', password: 'pass1' }
    const users = [user]

    const article = { 
        id: '1', title: 'mock title', date: '1970-01-01', image: 'https://',
        author: { name: 'mock author', mail: 'mock@mail.com' },
        content : 'paragraph 1\nparagraph 2\n\paragraph 3',
        labels: []
    }
    const articles = [ article ]

    function mockFetch() {
        cy.intercept('GET', '/users.json', users)
        cy.intercept('GET', '/articles.json', articles)
    }

    it(
        'Si al entrar, las credenciales guaradas localStorage NO son validas, eliminar las credenciales guardadas del localStorage y redidigir al usuario a la página de login.',
        () => {
            mockFetch()
            localStorage.clear()
            localStorage.setItem('user', user.username)
            localStorage.setItem('session', 'abcde')

            cy.visit(`${URL}?id=${article.id}`)
            
            cy.location('pathname').should('contain', 'login.html')
            cy.window().its('localStorage').invoke('getItem', 'user').should('eq', null)
            cy.window().its('localStorage').invoke('getItem', 'session').should('eq', null)
        }
    )
})
