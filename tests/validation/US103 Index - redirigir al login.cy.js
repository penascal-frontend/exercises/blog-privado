describe(
  'US103 Index - redirigir al login',
  () => {
    const URL = Cypress.env('URL')

    it(
      'Debe de tener un link al login del blog',
      () => {
        cy.visit(URL)
        cy.get('a[href^="login.html"]').should('exist')
      }
    )

  }
)