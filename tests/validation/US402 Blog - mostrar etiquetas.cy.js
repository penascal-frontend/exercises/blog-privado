describe('US402 Blog - mostrar etiquetas', () => {
    const URL = `${Cypress.env('URL')}/blog.html`

    const user = { username: 'user1', password: 'pass1' }
    const session_id = '3yz'
    const users = [user]

    const article = { 
        id: '1', title: '', date: '1970-01-01', image: '',
        author: { name: '', mail: '' },
        content : '',
        labels: []
    }
    const articles = [
        {...article, labels:['a', 'b']},
        {...article, labels:['b', 'c']},
        {...article, labels:['c', 'd']},
        {...article, labels:['a', 'c']},
    ]

    function getLabels(){
        return Array.from(new Set(
            articles.reduce((acum, elem) => [...acum, ...elem.labels], [])
        )).sort()
    }

    function mockFetch() {
        cy.intercept('GET', '/users.json', users)
        cy.intercept('GET', '/articles.json', articles)
    }

    beforeEach(() => {
        mockFetch()
        localStorage.setItem('user', user.username)
        localStorage.setItem('session', session_id)
    })

    it(
        'En el nav, se mostrarán todas las etiquetas como elementos seleccionables / clicables',
        () => {
            cy.visit(URL)
            cy.get('nav input[type=radio]')
                .should('have.length', articles.length + 1)
                .then(inputs => {
                    const readed = Array.from(inputs.map(idx => inputs[idx].value))
                    const expected = ['__all__', ...getLabels()]
                    expect(readed).to.deep.equal(expected)
                })
        }
    )

    it(
        'Al seleccionar una etiqueta, los artículos se filtrarán mostrando únicamen los que contengan esa etiqueta',
        () => {
            cy.visit(URL)
            for (const label of getLabels()) {
                cy.get(`label:has(input[type=radio][value="${label}"])`).click()
                cy.get(`main article span`)
                .each(span => {
                    const labels = span.text().split(', ')
                    expect(label).to.be.oneOf(labels)
                })
            }
        }
    )

    it(
        'Siempre deberá aparecer una primera opción la cual elimine cualquier filtro.',
        () => {
            cy.visit(URL)
            for (const label of getLabels()) {
                cy.get(`label:has(input[type=radio][value="${label}"])`).click()
                cy.get('label:has(input[type=radio][value="__all__"])').click()
                cy.get(`main article`).should('have.length', articles.length)
            }
        }
    )

    it(
        'El usuario deberá diferenciar visualmente qué filtro está aplicado.',
        () => {
            cy.visit(URL)
            const labels = ['__all__', ...getLabels()]
            const bold = '700'
            for (const label of labels) {
                const selector = `label:has(input[type=radio][value="${label}"])`
                cy.get(selector)
                    .click()
                    .invoke('css', 'font-weight')
                    .then(fontWeight => expect(fontWeight).to.eq(bold))
            }
        }
    )

})
