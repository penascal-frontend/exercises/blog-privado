describe('US203 Login - validar sesion', () => {
    const URL = `${Cypress.env('URL')}/login.html`
    const user = { username: 'user1', password: 'pass1' }
    const users = [user]

    function mockUsers() {
        cy.intercept('GET', '/users.json', users)
    }

    function makeSession(user) {
       return (user.username + user.password).split('').reduce(
            (acum, char, idx) => acum + (idx + 1) * char.charCodeAt(0), 0
        ).toString(36)
    }

    it(
        'Si al entrar, las credenciales guaradas localStorage SI son validas, redirigir directamente al blog.',
        () => {
            mockUsers()
            localStorage.clear()
            localStorage.setItem('user', user.username)
            localStorage.setItem('session', makeSession(user))
            cy.visit(URL)
            cy.location('pathname').should('contain', 'blog.html')
        }
    )

    it(
        'Si al entrar, las credenciales guaradas localStorage NO son validas, redirigir directamente al blog.',
        () => {
            mockUsers()
            localStorage.clear()
            localStorage.setItem('user', user.username)
            localStorage.setItem('session', 'abcde')
            cy.visit(URL)
            cy.location('pathname').should('contain', 'login.html')
            cy.window().its('localStorage').invoke('getItem', 'user').should('eq', null)
            cy.window().its('localStorage').invoke('getItem', 'session').should('eq', null)
        }
    )
})
