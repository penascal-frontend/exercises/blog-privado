describe(
  'US102 Index - redirigir al repositorio',
  () => {
    const URL = Cypress.env('URL')
    
    it(
      'La página index debe de tener un link que muestre el repositorio en otra pestaña',
      () => {
        cy.visit(URL)
        cy.get('a[href^="https://"][target="_blank"]').should('exist')
      }
    )

  }
)