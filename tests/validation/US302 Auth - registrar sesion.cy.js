describe('US302 Auth - registrar sesion', () => {
    const URL = `${Cypress.env('URL')}/auth.html`
    const user = { username: 'user1', password: 'pass1' }
    const users = [user]

    function mockUsers() {
        cy.intercept('GET', '/users.json', users)
    }

    function makeSession(user) {
       return (user.username + user.password).split('').reduce(
            (acum, char, idx) => acum + (idx + 1) * char.charCodeAt(0), 0
        ).toString(36)
    }

    it(
        'Si la validación SI tiene éxito se almacenarán las claves user y session en localStorage',
        () => {
            mockUsers()
            cy.visit(`${URL}?username=${user.username}&password=${user.password}`)
            cy.window().its('localStorage').invoke('getItem', 'user').should('eq', user.username)
            cy.window().its('localStorage').invoke('getItem', 'session').should('eq', makeSession(user))
        }
    )

    it(
        'Si la validación NO tiene éxito se eliminaran las claves user y session en localStorage',
        () => {
            mockUsers()
            cy.visit(`${URL}?username=${user.username}&password=abcde`)
            cy.window().its('localStorage').invoke('getItem', 'user').should('eq', null)
            cy.window().its('localStorage').invoke('getItem', 'session').should('eq', null)
        }
    )
})
