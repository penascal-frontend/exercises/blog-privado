describe('US403 Blog - validar sesion', () => {
    const URL = `${Cypress.env('URL')}/blog.html`
    const user = { username: 'user1', password: 'pass1' }
    const users = [user]

    function mockUsers() {
        cy.intercept('GET', '/users.json', users)
    }

    it(
        'Si al entrar, las credenciales guaradas localStorage NO son validas, eliminar las credenciales guardadas del localStorage y redidigir al usuario a la página de login.',
        () => {
            mockUsers()
            localStorage.clear()
            localStorage.setItem('user', user.username)
            localStorage.setItem('session', 'abcde')
            cy.visit(URL)
            cy.location('pathname').should('contain', 'login.html')
            cy.window().its('localStorage').invoke('getItem', 'user').should('eq', null)
            cy.window().its('localStorage').invoke('getItem', 'session').should('eq', null)
        }
    )
})
