describe('US504 Article - cerrar sesion', () => {
    const URL = `${Cypress.env('URL')}/article.html`

    const user = { username: 'user1', password: 'pass1' }
    const session_id = '3yz'
    const users = [user]

    const article = { 
        id: '1', title: 'mock title', date: '1970-01-01', image: 'https://',
        author: { name: 'mock author', mail: 'mock@mail.com' },
        content : 'paragraph 1\nparagraph 2\n\paragraph 3',
        labels: []
    }
    const articles = [ article ]

    function mockFetch() {
        cy.intercept('GET', '/users.json', users)
        cy.intercept('GET', '/articles.json', articles)
    }

    beforeEach(() => {
        mockFetch()
        localStorage.setItem('user', user.username)
        localStorage.setItem('session', session_id)
    })

    it('El usuario deberá de confirmar que realmente quiere cerrar la sesion', async () => {
        cy.on('window:confirm', text => {
            expect(text).to.eq('Salir del blog?')
            return false
        })
        cy.visit(URL)
        
        cy.get('a#logout').click()
    })

    it('El usuario deberá de confirmar que realmente quiere cerrar la sesion', async () => {
        cy.on('window:confirm', cy.stub().returns(false))
        
        cy.visit(URL)
        cy.get('a#logout').click()
        cy.window().its('localStorage').invoke('getItem', 'session').should('eq', session_id)
    })

    it('Al cerrar la sesión, las credenciales guardadas en localStorage, se deben eliminar', async () => {
        cy.on('window:confirm', cy.stub().returns(true))
        
        cy.visit(URL)
        cy.get('a#logout').click()
        cy.window().its('localStorage').invoke('getItem', 'user').should('eq', null)
        cy.window().its('localStorage').invoke('getItem', 'session').should('eq', null)
    })


    it('Al cerrar la sesión, las el usuario será redirigido a la página principal', async () => {
        cy.on('window:confirm', cy.stub().returns(true))
        
        cy.visit(URL)
        cy.get('a#logout').click()
        cy.location('pathname').should('contain', 'index.html')
    })
})

