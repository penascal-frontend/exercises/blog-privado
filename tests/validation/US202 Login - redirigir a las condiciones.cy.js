describe(
  'US202 Login - redirigir a las condiciones',
  () => {
    const URL = Cypress.env('URL')

    it(
      'Se deben mostrar las condiciones en una página aparte',
      () => {
        cy.visit(`${URL}/login.html`)
        cy.get('a[href^="terms.html"][target="_blank"]').should('exist')
      }
    )

  }
)