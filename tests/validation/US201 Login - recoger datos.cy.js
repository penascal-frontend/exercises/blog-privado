describe(
  'US201 Login - recoger datos',
  () => {
    const URL = Cypress.env('URL')
    let form 

    before(() => {
      cy.visit(`${URL}/login.html`)
      form = cy.get('form')
    }

    )
    it(
      'Debe tener un formulario cuya action es auth.html',
      () => {
        form.invoke('attr', 'action').should('eq', 'auth.html')
      }
    )

    it(
      'Debe tener un formulario cuyo method es GET',
      () => {
        form.invoke('attr', 'method').should('eq', 'GET')
      }
    )

    it(
      'El formulario ha de tener un input tipo text para el usuario',
      () => {
        form.find('input[type="text"][name="username"]').should('exist')
      }
    )

    it(
      'El formulario ha de tener un input tipo password para la clave',
      () => {
        form.get('input[type="password"][name="password"]').should('exist')
      }
    )

    it(
      'El formulario ha de tener un input tipo checkbox para la aceptación de las condiciones',
      () => {
        form.get('input[type="checkbox"]').should('exist')
      }
    )

  }
)
