describe('US401 Blog - mostrar articulos', () => {
    const URL = `${Cypress.env('URL')}/blog.html`

    const user = { username: 'user1', password: 'pass1' }
    const session_id = '3yz'
    const users = [user]

    const article = { 
        id: '1', title: '', date: '1970-01-01', image: '',
        author: { name: '', mail: '' },
        content : '',
        labels: []
    }
    const articles = [article, article, article, article]

    function mockFetch() {
        cy.intercept('GET', '/users.json', users)
        cy.intercept('GET', '/articles.json', articles)
    }

    beforeEach(() => {
        mockFetch()
        localStorage.setItem('user', user.username)
        localStorage.setItem('session', session_id)
    })

    it(
        'En el main, se mostrarán todos los artículos',
        () => {
            cy.visit(URL)
            cy.get('main article').should('have.length', articles.length)
        }
    )
})
