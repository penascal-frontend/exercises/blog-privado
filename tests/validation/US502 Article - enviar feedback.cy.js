describe('US502 Article - mostrar contenido', () => {
    const URL = `${Cypress.env('URL')}/article.html`

    const user = { username: 'user1', password: 'pass1' }
    const session_id = '3yz'
    const users = [user]

    const article = { 
        id: '1', title: 'mock title', date: '1970-01-01', image: 'https://',
        author: { name: 'mock author', mail: 'mock@mail.com' },
        content : 'paragraph 1\nparagraph 2\n\paragraph 3',
        labels: []
    }
    const articles = [ article ]

    function mockFetch() {
        cy.intercept('GET', '/articles.json', articles)
    }

    beforeEach(() => {
        mockFetch()
        localStorage.setItem('user', user.username)
        localStorage.setItem('session', session_id)
    })

    it(
        'Que haya un link que abra un email en la aplicacion por defecto del dispositivo con la referencia del artículo precargada',
        () => {
            cy.visit(`${URL}?id=${article.id}`)

            cy.get('#feedback')
                .invoke('attr', 'href')
                .should('match', /^mailto:/)
                .should('include', encodeURI(article.title))
        }
    )
})
