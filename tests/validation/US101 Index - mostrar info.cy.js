describe(
  'Validación de US101 Index - mostrar info',
  () => {
    const URL = Cypress.env('URL')
    function getArticles() {
      cy.visit(URL)
      const seccion = cy.get('#info')
      return seccion.find('article')
    }

    it(
      'Debe de tener una seccion con varios artículos ...',
      () => {
        getArticles().should('have.length.greaterThan', 1)
      }
    )

    it(
      '... en los que se explican las bondades del blog',
      () => {
        getArticles().each(article => 
          cy.wrap(article[0].innerText).should('have.length.gt', 2)
        )
      }
    )

  }
)