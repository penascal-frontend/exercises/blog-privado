describe('US301 Auth - validar credenciales', () => {
    const URL = `${Cypress.env('URL')}/auth.html`

    function mockUsers() {
        const users = [{ username: 'user1', password: 'pass1' }]
        cy.intercept('GET', '/users.json', users)
    }

    it('Si la validación SI tiene éxito se redirigirá al usuario a blog.html', () => {
        mockUsers()
        cy.visit(`${URL}?username=user1&password=pass1`)
        cy.location('pathname').should('contain', 'blog.html')
    })

    it('Si la validación NO tiene éxito se redirigirá al usuario a login.html', () => {
        mockUsers()
        cy.visit(`${URL}?username=user1&password=pass2`)
        cy.location('pathname').should('contain', '/login.html')
    })
})
