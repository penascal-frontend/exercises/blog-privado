describe('US501 Article - mostrar contenido', () => {
    const URL = `${Cypress.env('URL')}/article.html`

    const user = { username: 'user1', password: 'pass1' }
    const session_id = '3yz'
    const users = [user]

    const article = { 
        id: '1', title: 'mock title', date: '1970-01-01', image: 'https://',
        author: { name: 'mock author', mail: 'mock@mail.com' },
        content : 'paragraph 1\nparagraph 2\n\paragraph 3',
        labels: []
    }
    const articles = [ article ]

    function mockFetch() {
        cy.intercept('GET', '/articles.json', articles)
        cy.intercept('GET', '/users.json', users)
    }

    beforeEach(() => {
        mockFetch()
        localStorage.setItem('user', user.username)
        localStorage.setItem('session', session_id)
    })

    it(
        'Que se cargue el contenido del artículo en sus correspondientes contenedores',
        () => {
            const formatter = new Intl.DateTimeFormat('es-ES', { dateStyle: 'full' })
            const expected_date = formatter.format(new Date(article.date))

            cy.visit(`${URL}?id=${article.id}`)
            cy.get('#image')
              .should('have.attr', 'src', article.image)
              .should('have.attr', 'alt', article.title)
            cy.get('#date').should('have.text', expected_date)
            cy.get('#author').should('have.text', article.author.name)
            cy.get('main h2').should('have.text', article.title)
            cy.get('main p').then(paragraphs => {
                expect(paragraphs).to.have.lengthOf(article.content.split('\n').length)
                const text = Array.from(paragraphs).map(p => p.innerText).join('\n')
                expect(text).to.equal(article.content)
            })
        }
    )
})
