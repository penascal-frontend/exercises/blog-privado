import { checkSession, clearSession } from '@/libs/session'
import { getArticles, getLabels } from '@/libs/article'

function renderArticle(article){
    const date = new Date(article.date)
    const formatter = new Intl.DateTimeFormat('es-ES', { year: 'numeric' })
    const date_string = formatter.format(date)

    return `<article id=${article.id}>
        <h2>${article.title}</h2>
        <h3>${article.author.name} - ${date_string}</h3>
        <picture>
            <img src="${article.image}" alt="imagen del libro ${article.title} de ${article.author.name}">
        </picture>
        <span>${article.labels.join(', ')}</span>
    </article>`
}

function renderLabel(label){
    return `<li><label>
        <input type="radio" name="label" value="${label}">
        ${label == '__all__' ? 'Mostrar Todos' : label}
    </label></li>`
}

async function applyLabelFilter(event) {
    const label = event.target.value
    const articles = await getArticles(label == '__all__' ? '' : label)
    document.getElementById('articles').innerHTML = articles.map(renderArticle).join('')
    document.querySelectorAll('#articles article').forEach(article => 
        article.addEventListener('click', createShowArticleHandler(article.id))
    )
}

async function validateSession(){
    const user = localStorage.getItem('user')
    const session = localStorage.getItem('session')

    if (!user || !session) return
    return await checkSession(user, session)
}

async function logout() {
    const ok = confirm('Salir del blog?')
    if (!ok) return 
    clearSession()
    location.replace('index.html')
}

function createShowArticleHandler(id) {
    return function handler () {
        location.href = `article.html?id=${id}`
    }
}

async function main() {
    document.getElementById('logout').addEventListener('click', logout)

    const session_ok = await validateSession()
    if (!session_ok) {
        localStorage.clear()
        location.replace('login.html')
    }

    const articles = await getArticles()
    const labels = [
        '__all__',
        ...(await getLabels())
    ]

    document.getElementById('articles').innerHTML = articles.map(renderArticle).join('')
    document.getElementById('labels').innerHTML = labels.map(renderLabel).join('')

    document.querySelectorAll('#labels input[type=radio]').forEach(
        input => input.addEventListener('input', applyLabelFilter)
    )
    const mainFilter = document.querySelector('#labels input[type=radio][value="__all__"')
    mainFilter.click()
}

main()