import { checkSession, clearSession } from '@/libs/session'
import { getArticles } from '@/libs/article'

function renderArticle(article){
    const formatter = new Intl.DateTimeFormat('es-ES', { dateStyle: 'full' })
    document.getElementById('date').innerText = formatter.format(new Date(article.date))

    document.getElementById('author').innerText = article.author.name

    const image = document.getElementById('image')
    image.src = article.image
    image.alt = article.title

    const title = document.createElement('h2')
    title.innerText = article.title

    const main = document.querySelector('main')
    main.innerText = ''
    main.appendChild(title)

    for (const paragraph of article.content.split('\n')) {
        const p = document.createElement('p')
        p.innerText = paragraph
        main.appendChild(p)
    }

}

function getArticleID(search_string) {
    const params = new URLSearchParams(search_string)
    return params.get('id')
}

async function getArticle(id) {
    const articles = await getArticles()
    return articles.find(article => article.id == id)
}

function prepareFeedbackLink(anchor, article) {
    const to = article.author.mail
    const subect = `Feedback sobre el articulo "${article.title}"`
    const body = `Hola,
    Remito este comentario sobre el articulo "${article.title}" desde su página del blog.

    Me gustaría cometar que .... 

    `
    anchor.href = `mailto:${to}?subject=${encodeURI(subect)}&body=${encodeURI(body)}`
}

async function validateSession(){
    const user = localStorage.getItem('user')
    const session = localStorage.getItem('session')

    if (!user || !session) return
    return await checkSession(user, session)
}

async function logout() {
    const ok = confirm('Salir del blog?')
    if (!ok) return 
    clearSession()
    location.replace('index.html')
}

async function main() {
    const session_ok = await validateSession()
    if (!session_ok) {
        localStorage.clear()
        location.replace('login.html')
    }

    const id = getArticleID(location.search)
    if (!id) {
        location.replace('blog.html')
        return
    }

    const article = await getArticle(id)
    if (!article) {
        location.replace('blog.html')
        return
    }
    
    renderArticle(article)

    const feedback = document.getElementById('feedback')
    prepareFeedbackLink(feedback, article)

    document.getElementById('logout').addEventListener('click', logout)
}

main()