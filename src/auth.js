import { getUsers, getCredentialsFromParams, validateCredentials } from "@/libs/user"
import { setSession, makeSession, clearSession } from "@/libs/session"


async function main() {
    const { username, password } = getCredentialsFromParams(location.search)
    const users = await getUsers()
    const ok = validateCredentials(users, username, password)

    if (ok) {
        setSession(username, makeSession({ username, password }))
        location.replace('blog.html')
    } else {
        clearSession()
        location.replace('login.html')
    }
}

main()
