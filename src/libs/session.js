import { getJsonData } from './data'

function setSession(username, session) {
    localStorage.setItem('user', username)
    localStorage.setItem('session', session)
}

function clearSession() {
    localStorage.clear()
}

function makeSession(user) {
    return (user.username + user.password).split('').reduce(
        (acum, char, idx) => acum + (idx + 1) * char.charCodeAt(0), 0
    ).toString(36)
}

async function checkSession(username, session) {
    const users = await getJsonData('users.json')
    const user = users.find(u => u.username === username)
    if (!user) return false
    return session === makeSession(user)
}

export { makeSession, setSession, clearSession, checkSession }
