function memoizedGetJsonDataFactory() {
    const cache = {}


    async function getJsonData(url) {
        if (!!cache?.[url]) return cache[url]

        const response = await fetch(url)
        if (!response.ok) return null

        const res = await response.json()
        cache[url] = res

        return res
    }

    return getJsonData
}

const memoizedGetJsonData = memoizedGetJsonDataFactory()

export { memoizedGetJsonData as getJsonData }
