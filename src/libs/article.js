import { getJsonData } from './data'

async function getArticles(label) {
    const articles = await getJsonData('articles.json')
    return articles.filter(article => !label || article.labels.includes(label))
}

async function getLabels() {
    const articles = await getJsonData('articles.json')
    return Array.from(new Set(
        articles.reduce((acum, elem) => [...acum, ...elem.labels], [])
    )).sort()
}

export { getArticles, getLabels }
