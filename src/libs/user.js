import { getJsonData } from './data'

async function getUsers() {
    return await getJsonData('users.json')
}

function validateCredentials(users, username, password) {
    if (!users) return false
    for (const user of users) {
        if (user.username === username && user.password === password) {
            return true
        }
    }
    return false
}

function getCredentialsFromParams(search_string) {
    const params = new URLSearchParams(search_string)
    const username = params.get('username')
    const password = params.get('password')
    return { username, password }
}


export { getUsers, validateCredentials, getCredentialsFromParams }
