import { checkSession } from '@/libs/session'

function manageButtonState(button) {
    function changeEventHandler(event) {
        button.disabled = !event.target.checked
    }
    return changeEventHandler
}

async function validateSession(){
    const user = localStorage.getItem('user')
    const session = localStorage.getItem('session')

    if (!user || !session) return
    return await checkSession(user, session)
}

function main() {
    validateSession().then(res => {
        if (res) location.replace('blog.html')
        else localStorage.clear()
    })

    const terms = document.getElementById('terms')
    const login = document.getElementById('login')

    terms.addEventListener('change', manageButtonState(login))
}

main()
